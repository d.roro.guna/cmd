import java.util.Scanner;
import java.nio.file.Path;

public class Main {

    public static void main(String[] args) {
        String respuesta;
            do {
                Scanner sc = new Scanner(System.in);respuesta = sc.nextLine();
                String[] partido=respuesta.split(" ");

                switch (partido[0]) {
                    case "help":
                        Metodos.help(partido);
                        break;
                    case "cd":
                        Metodos.cd(partido);
                        break;
                    case "mkdir":
                        Metodos.mkdir(partido);
                        break;
                    case "info":
                        Metodos.info(partido);
                        break;
                    case "cat":
                        Metodos.cat(partido);
                        break;
                    case "top":
                        Metodos.top(partido);
                        break;
                    case "mkfile":
                        Metodos.mkfile(partido);
                        break;
                    case "delete":
                        Metodos.delete(partido);
                        break;
                    case "dir":
                        Metodos.dir(partido);
                        break;
                    case "readPoint":
                        Metodos.readPoint(partido);
                        break;
                    case "start":
                        Metodos.start(partido);
                        break;
                    case "close":
                        Metodos.close(partido);
                        break;
                    case "clear":
                        Metodos.clear();
                        break;
                    default:
                        System.out.println("Opción no válida");
                        break;

                }
            }while(!respuesta.equals("close"));

        }
    }
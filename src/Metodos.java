import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class Metodos {

    public static String directorioAct = System.getProperty("user.dir");

    /******************************************************************************************************/
    public static void help(String[] partido) {
        if (partido.length == 1) {
            System.out.println("CD             Muestra el nombre del directorio actual o cambia a otro\n" +
                    "               directorio." + "\n" + "MKDIR          Crea un directorio." + "\n" + "info <nombre> -> Muestra la información del elemento Indicando:" + "\n" + "cat <nombreFichero> -> Muestra el contenido de un fichero." + "\n" + "top <numeroLineas><NombreFichero> -> Muestra las líneas especificadas de un\n" +
                    "fichero.\"" + "\n" + "mkfile <nombreFichero> <texto> -> Crea un fichero con ese nombre y el contenido de\n" +
                    "texto." + "\n" + "write <nombreFichero> <texto>-> Añade 'texto' al final del fichero especificado." + "\n" + "dir -> Lista los archivos o directorios de la ruta actual." + "\n" + "readpoint <nombreFichero1> <posición> \uF0E0 Lee un archivo desde una determinada\n" +
                    "posición del puntero" + "\n" + "delete <nombre>-> Borra el fichero, si es un directorio borra todo su contenido y a si\n" +
                    "mismo." + "\n" + "Start <programa> ejecuta este proceso" + "\n" + "close-> Cierra el programa." + "\n" + "Clear -> Vacía la lista");
        }else{
            System.out.println("El comando help no acepta parámetros");
        }


        }

    /******************************************************************************************************/
    public static void cd(String[] partido) {
        if (partido.length == 1) {
            System.out.println("Directorio actual: " + directorioAct);
        } else if (partido.length == 2) {
            if (partido[1].equals("..")) {
                Path rutaAct = Paths.get(directorioAct);
                Path parentPath = rutaAct.getParent();
                if (parentPath != null) {
                    directorioAct = parentPath.toString();
                    System.out.println(directorioAct);
                } else {
                    System.out.println("Ya estás en el directorio raíz.");
                }
            } else {

                Path ruta = Paths.get(directorioAct, partido[1]);
                if (ruta.toFile().exists() && ruta.toFile().isDirectory()){
                    directorioAct = ruta.toAbsolutePath().toString();
                    System.out.println(directorioAct.toString());
                }else{
                    System.out.println("La ruta no es un directorio");
                }

            }
        }
    }

    /******************************************************************************************************/
    public static void mkdir(String[] partido) {
        try {
            if (partido.length == 2) {
                File dir = new File(directorioAct + "\\" + partido[1]);
                if (dir.mkdir()) {
                    System.out.println("Directorio creado ");
                } else {
                    System.out.println("No se pudo crear, ya existe ");
                }
            } else {
                System.out.println("Uso incorrecto de mkdir, correcto: mkdir <nombreDirectorio>");
            }

        } catch (IndexOutOfBoundsException exception) {
            exception.getMessage();
        }
    }
    /******************************************************************************************************/
    public static void info(String[] partido) {
        if (partido.length ==2) {
            Path p = Path.of(directorioAct);
            File dir = new File(p.toString());
            if (dir.exists()) {
                long bytes = dir.length();
                long espacioTotal = dir.getTotalSpace();
                long espcioLibre = dir.getUsableSpace();
                System.out.println("Tamaño en bytes: " + bytes + "\n" + "`Espacio total " + espacioTotal + "\n" + " Espacio restante " + espcioLibre);
            } else {
                System.out.println("La ruta tiene que ser un directorio o un archivo");
            }
        } else {
            System.out.println("Uso incorrecto del comando info, uso correcto info <directorio>");
        }
    }
    /******************************************************************************************************/
    public static void cat(String[] partido) {
        if (partido.length == 2) {
            File dir = new File(directorioAct + "\\" + partido[1]);
            if (dir.isFile() && dir.exists()){
                int lineas;
                try {
                    FileReader archivo = new FileReader(dir);
                    BufferedReader leer = new BufferedReader(archivo);

                    String e;
                    while ((e = leer.readLine()) != null) {
                        System.out.println(e);
                    }
                    leer.close();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }else{
                System.out.println("El parámetro introducido tiene que ser un archivo");
            }
        }else{
            System.out.println("Uso incorrecto del comndo cat, correcto cat <archivo>");
        }
    }
    /******************************************************************************************************/
    public static void top(String[] partido) {
        if (partido.length == 3) {
            File dir = new File(directorioAct + "\\" + partido[2]);
            if (dir.isFile()) {
                int lineas = Integer.parseInt(partido[1]);

                try {
                    FileReader archivo = new FileReader(dir);
                    BufferedReader leer = new BufferedReader(archivo);

                    String e;
                    int contador = 0;
                    while ((e = leer.readLine()) != null && contador < lineas) {
                        System.out.println(e);
                        contador++;
                    }

                } catch (IOException e) {
                    System.out.println("No se ha encontrado el archivo especificado");
                    e.getMessage();
                }
            } else {
                System.out.println("Error, el tercer parámetro tiene que ser un archivo");
            }
        } else {
            System.out.println("Uso incorrecto del comando top, uso correcto cat <lineas a leer> <archivo>");
        }


    }
/******************************************************************************************************/
    public static void mkfile(String[] partido) {
        if (partido.length == 3) {
            File dir = new File(directorioAct + "\\" + partido[1]);
            if (!dir.exists()){
                try {
                    dir.createNewFile();
                    System.out.println("El archivo se ha creado");

                    FileWriter escribir = new FileWriter(dir);
                    BufferedWriter bw = new BufferedWriter(escribir);
                    bw.write(partido[2]);
                    bw.close();

                    System.out.println("Se ha escrito correctamente");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }else{
                try{
                    FileWriter escribir = new FileWriter(dir);
                    BufferedWriter bw = new BufferedWriter(escribir);
                    bw.write(partido[2]);
                    bw.close();

                    System.out.println("Se ha escrito correctamente");

                }catch (IOException e) {
                    System.err.println("Error, el segundo parámetro tiene que ser un archivo, o ya existe");
                    e.printStackTrace();
                }
            }
        }else{
            System.out.println("Uso incorrecto del comando mkfile, uso correcto mkfile <nombreFichero> <texto>");
        }
    }
    /******************************************************************************************************/
    public static void dir(String []partido){

        try {
            if (partido.length==1){
                Process proceso =Runtime.getRuntime().exec("cmd /c dir \"" +directorioAct + "\"");
                BufferedReader reader = new BufferedReader(new InputStreamReader(proceso.getInputStream()));

                String linea;
                while ((linea = reader.readLine()) != null) {
                    System.out.println(linea);
                }
            }else{
                System.out.println("El comando Dir solo acepta un parámetro");
            }
        } catch (IOException e) {
            System.out.println("No se pudo ejecutar el comando");
            throw new RuntimeException(e);
        }
    }
    /******************************************************************************************************/
    public static void readPoint(String[]partido){
        if (partido.length==3){
            File dir = new File(directorioAct + "\\" + partido[1]);
            if (dir.isFile()){
                long pos = Long.parseLong(partido[2]);
                try {
                    RandomAccessFile raf = new RandomAccessFile(dir,"r");
                    raf.seek(pos);
                    String linea;
                    while ((linea=raf.readLine())!=null){
                        System.out.println(linea);
                    }
                    raf.close();
                } catch (IOException e) {
                    System.out.println("Error, no se ha encontrado el archivo");
                    throw new RuntimeException(e);
                }
            }else {
                System.out.println("El segundo argumento tiene que ser un archivo");
            }
        }else{
            System.out.println("Uso incorrecto del comando readPoint, uso correcto readPoinr <nombreFichere> <posición>");
        }


    }
    /******************************************************************************************************/

    public static void deleteRecur(File directorio,String[]partido) {
        if (partido.length == 2) {
            File dir = new File(directorioAct + "\\" + partido[1]);
            if (dir.isFile() && dir.exists()) {
                dir.delete();
            } else if (dir.isDirectory() && dir.exists()) {
                File[] archivos = dir.listFiles();
                if (archivos != null) {
                    for (File archivo : archivos) {
                        deleteRecur(dir);
                    }
                }
            }
        }else{
            System.out.println("Uso incorrecto del comando delete, correcto delete <Archivo/Directorio> ");
        }
    }

                  public static void delete (String[]partido){
                    if (partido.length == 2) {
                        File dir = new File(directorioAct + "\\" + partido[1]);
                        if (dir.exists()) {
                            if (dir.isDirectory()) {
                                deleteRecur(dir);
                                System.out.println("Directorio eliminado con éxito");
                            } else {
                                dir.delete();
                                System.out.println("Archivo eliminado con exito");
                            }
                        } else {
                            System.out.println("El archivo/directorio no existe");
                        }
                    } else {
                        System.out.println("Uso incorrecto del comando delete, uso correcto delete <nombre archivo/carpeta>");
                    }


                }
                public static void deleteRecur (File directorio){
                    File[] directorios = directorio.listFiles();
                    if (directorios != null) {
                        for (File d : directorios) {
                            if (d.isDirectory()) {
                                deleteRecur(d);
                            } else {
                                d.delete();
                            }
                        }
                        directorio.delete();
                    }

                }

                /******************************************************************************************************/
                public static void start (String[]partido){
                    if (partido.length == 2) {
                        ProcessBuilder pb = new ProcessBuilder("cmd.exe", "/c", partido[1]);
                        try {
                            Process p = pb.start();
                        } catch (IOException e) {
                            System.out.println("No se pudo iniciar" + partido[1]);
                            throw new RuntimeException(e);
                        }
                    } else {
                        System.out.println("Uso incorrecto del comando start, uso correcto start <nombre proceso>");
                    }
                }
                /******************************************************************************************************/
                public static void close(String[]partido){
                    System.exit(0);
                }
                /******************************************************************************************************/
                public static void clear () {
                    System.out.println("                                                       ");
                    System.out.println("                                                       ");
                    System.out.println("                                                       ");
                    System.out.println("                                                       ");
                    System.out.println("                                                       ");
                    System.out.println("                                                       ");
                    System.out.println("                                                       ");
                    System.out.println("                                                       ");
                    System.out.println("                                                       ");
                    System.out.println("                                                       ");
                    System.out.println("                                                       ");
                    System.out.println("                                                       ");
                    System.out.println("                                                       ");
                    System.out.println("                                                       ");
                    System.out.println("                                                       ");
                    System.out.println("                                                       ");
                    System.out.println("                                                       ");
                    System.out.println(directorioAct);

                }
            }










